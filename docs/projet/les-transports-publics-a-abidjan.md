---
---
# Les transports publics à Abidjan

## Une métropole sans information unifiée de transport
Avec près de 4,4 millions d'habitants en 2014, Abidjan est la plus grande aire métropolitaine d'Afrique Subsaharienne.

Il n'existe pas actuellement de plan à jour regroupant les lignes de transport, ni d'application de calcul d'itinéraire en transports en commun. Pour planifier un déplacement, la population d'Abidjan repose donc sur les renseignements du personnel des transports publics et sur les connaissances des autres usagers pour planifier un déplacement.

## Des transports variés et complémentaires

Les services de transport qui irriguent Abidjan sont composés d'une multitude d'acteurs différents. Ceux-ci peuvent être des services de transport publics conventionnés, dont le financement est partiellement pris en charge par les pouvoirs publics, tout comme des services de transport non conventionnés, qui ne sont pas pris en charge par les pouvoirs publics.

Les services de transport conventionnés regroupent les bus de la SOTRA et les navettes lagunaires de la Citrans et de STL. Ces trois acteurs se partagent les lignes.

![Navette Aqualines et bus SOTRA](../images/aqualines-sotra.jpg)
*Navette Aqualines et bus SOTRA*

Les services de transport non conventionnés regroupent les minibus Gbakas et les taxis collectifs Wôro-wôros. Ces catégories comprennent des centaines d'acteurs différents, fédérées en syndicats de transports mais dont la gestion n'est pas unifiée.

![Minibus Gbaka et taxi collectif Wôrô-wôrô](../images/gbaka-woro.png)
*Minibus Gbaka et taxi collectif Wôrô-wôrô*

L'ensemble de ces offres de transport sont nécessaires pour assurer les déplacements des Abidjanais. Ils agissent de manière complémentaire : les Gbakas assurent des dessertes intercommunales là où la demande dépasse l'offre conventionnée et les Wôrô-wôrôs assurent le report et la diffusion intra-communale.

On estime en 2007 que les services Gbakas mobilisent un parc dépassant les 4 000 véhicules, et les services Wôrô-wôrô dépassent les 11 000 véhicules. En 2007, plus de la moitié des véhicules de ces catégories ne disposent pas d'autorisation de transport ni de visite technique.

## La difficile unification de l'information
Les services non conventionnés regroupent une multitude d'exploitants. D'après les études menées en 2005 sur le parc de Wôrô-wôrô à Yopougon et Cocody, 4 véhicules sur 5 appartiennent à un propriétaire isolé, qui n'a qu'un seul véhicule et trouve un chauffeur pour l'exploiter ou l'exploite lui-même. Les propriétaires sont fédérés dans de nombreux syndicats de transporteurs, coordonnés par le Haut Conseil du Patronat des Entreprises de Transport Routier de Côte d'Ivoire.

Les interlocuteurs administratifs sont également pluriels : les mairies coordonnent les services de transport non conventionnés la Direction Générale des Transports Terrestres et de la Circulation du Ministère des Transports coordonne les transports conventionnés et oriente les interactions entre les différents modes.

Unifier l'information d'offre de transport au sein de cette multitude d'acteurs est donc difficile. Les petits exploitants de Gbakas ou Wôrô-wôrô ne disposent pas de moyens leur permettant d'éditer un fichier GTFS qui permettrait le partage d'information. C'est pourquoi le ministère du transport a lancé le projet de cartographie, première étape d'une démarche de référencement de l'offre multimodale de transport public d'Abidjan.
