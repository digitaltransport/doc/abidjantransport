---
---

# Présentation du projet

Le projet de cartographie du transport conventionné et non conventionné à Abidjan est lancé par le [Ministère du transport de Côte d'Ivoire](http://www.transports.gouv.ci/) avec l'aide d'un financement [AFD](https://www.afd.fr/) dans son initiative [DigitalTransport4Africa](https://digitaltransport4africa.org/).

L'objectif est de spécifier l'offre de transport en commun d'Abidjan et se concentre sur trois modes : les bus, les navettes lagunaires et les Gbakas.

La production s'appuie sur l'[association locale des contributeurs OpenStreetMap de Côte d'Ivoire](http://openstreetmap.ci/), outillée par [Jungle Bus](https://junglebus.io/) et pilotée par [SYSTRA](https://www.systra.com/fr/).

Un site vitrine permet d'accéder rapidement aux différents livrables du projet : [http://sites.digitaltransport.io/abidjantransport/](http://sites.digitaltransport.io/abidjantransport/)

Les données du projet sont hébergées sur le dépôt git de DigitalTransport4Africa dédié au projet : [https://git.digitaltransport4africa.org/data/africa/abidjan](https://git.digitaltransport4africa.org/data/africa/abidjan).


Le projet a été lancé en septembre 2019. La collecte et le traitement de données permettant la création du GTFS se sont poursuivis jusqu'à mars 2020. Une collecte complémentaire en septembre 2020 a permis d'améliorer la complétude des données fournies.

![Les différentes entités intervenant sur le projet](../images/logos.png)
*Les différentes entités intervenant sur le projet*

# Ressources

* Les [supports](https://git.digitaltransport4africa.org/data/africa/abidjan/tree/master/Pr%C3%A9sentations%20du%20projet) utilisés pour présenter le projet au cours de différents événements sont disponibles sur le centre de ressources DigitalTransport4Africa
* Un [cahier des charges type](https://git.digitaltransport4africa.org/doc/tor) réutilisable par les autorités locales de transport de grandes villes africaines souhaitant mandater des prestataires pour la création de données de transport urbain a été créé par l'AFD à partir de celui de ce projet
