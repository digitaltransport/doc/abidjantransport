---
home: false
footer: AbidjanTransport

---
# Création de données GTFS pour le transport public à Abidjan

Une base de savoirs publique pour l'offre de transport conventionné et non conventionné à Abidjan.


De septembre 2019 à décembre 2020, un projet de création de données d'offre de transport a été mené à Abidjan. Ce document en résume les méthodes et caractéristiques.
Ces documentations, ainsi que les outils utilisés sont sous licence libre, et les données produites sont également ouvertes.
Vous trouverez ici une description du projet, de la méthodologie, des moyens mis en œuvre pour sa gestion et enfin un retour d'expérience.

[Présentation](/projet/presentation.md)
