module.exports = {
    title: 'AbidjanTransport',
    description: 'Cartographier les transports publics du Grand Abidjan',
    dest: 'public',
    base: "/abidjantransport/",
    themeConfig: {
      sidebar: [
        ['/', 'Accueil'],
        {
          title: 'PROJET',
          sidebarDepth: 0,
          collapsable: false,
          children: [
            '/projet/presentation.md',
            '/projet/les-transports-publics-a-abidjan.md',
          ]
        },
        {
          title: 'MÉTHODOLOGIE TECHNIQUE',
          sidebarDepth: 0,
          collapsable: false,
          children: [
            '/methodologie-technique/approche-generale.md',
            '/methodologie-technique/collecte-a-bord.md',
            '/methodologie-technique/collecte-en-gare.md',
            '/methodologie-technique/retranscription.md',
            '/methodologie-technique/export.md',
          ]
        },
        {
          title: 'GESTION / ENCADREMENT',
          sidebarDepth: 0,
          collapsable: false,
          children: [
            '/gestion-encadrement/implication-des-parties-prenantes.md',
            '/gestion-encadrement/formation-des-collecteurs.md',
            '/gestion-encadrement/formation-des-contributeurs.md',
            '/gestion-encadrement/suivi-avancement.md',
          ]
        },
      ]
    }
  }
