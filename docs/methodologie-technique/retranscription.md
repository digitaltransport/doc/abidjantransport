# Retranscription dans OSM

Les données collectées sont ensuite retranscrites dans OpenStreetMap par des contributeurs. Deux contributeurs ont été recrutés sur la base de leurs connaissances et compétences pré-existantes en édition OpenStreetMap, et formés par Jungle Bus à la création de données transport.

Un trajet dans OpenStreetMap est une collection ordonnée d'objets pré-existants : les routes empruntées par le véhicule, ainsi que les arrêts où il s'est arrêté. Chaque trajet est d'abord créé dans OpenStreetMap à partir de la trace GPS correspondante. Les éventuels arrêts manquants sont alors créés et les métadonnées propres au trajet (mode de transport, prix, fréquence de passage estimée auprès du conducteur, etc) sont renseignées. Les trajets sont ensuite regroupés en ligne de transport. Lorsqu'une collecte en gare a permis d'obtenir des informations horaires plus détaillées, ces informations sont alors ajoutées aux lignes. Enfin, la durée de chaque trajet est calculée à partir de la trace GPS et ajoutée à OpenStreetMap.

Divers points de contrôle (directement implémentés dans les outils d'édition) permettent de vérifier la qualité et l'exhaustivité des données ainsi produites.

Le détail des attributs (tags OSM) utilisés pour les différents objets a été défini avec les contributeurs locaux afin de tenir compte des spécificités locales, et est rappelé sur la [page de wiki du projet](https://wiki.openstreetmap.org/wiki/FR:WikiProject_C%C3%B4te_d%27Ivoire/Transport_Abidjan#Comment_cartographier_.3F).

![josm](../images/josm.png)

*les rues empruntées par une ligne de bus, en surbrillance dans l'outil d'édition JOSM*


## Outillage

L'outil d'édition principal pour cette étape est [JOSM](https://josm.openstreetmap.de/wiki/Fr%3AWikiStart), un client lourd d'édition OSM extrêmement utilisé. Plusieurs réglages lui sont appliqués afin de faciliter le travail :
* le [greffon PT Assistant](https://wiki.openstreetmap.org/wiki/FR:JOSM/Greffons/PT_Assistant) est activé afin d'améliorer la visualisation et l'édition des données de transport
* le [pré-réglage Jungle Stops](https://github.com/Jungle-Bus/josm-presets) est activé afin de permettre la création en un clic les différents types d'arrêts à partir de la trace GPS
Le détail des configurations de JOSM est détaillé sur la [page de wiki du projet](https://wiki.openstreetmap.org/wiki/FR:WikiProject_C%C3%B4te_d%27Ivoire/Transport_Abidjan#JOSM).

L'outil [Voodoo](http://junglebus.io/voodoo/) a été mis à disposition afin de gagner du temps lors de la création du trajet : il permet de pré-sélectionner dans JOSM les rues qui correspondent le plus à une trace GPS donnée, en réalisant une opération de [map-matching](https://en.wikipedia.org/wiki/Map_matching).

Les données sur les fréquences de passage et les créneaux des heures creuses et de pointe sont ajoutées en utilisant la [fonction de contrôle à distance de JOSM](https://josm.openstreetmap.de/wiki/Help/Preferences/RemoteControl). L'outil [Busy Hours](https://jungle-bus.github.io/Busy-Hours/) permet de les visualiser facilement afin de s'assurer de leur bonne mise en forme.

![Busy Hours](../images/busy-hours.png)

*Créneaux horaires et fréquences de passage d'une ligne de transport dans l'outil Busy Hours*

Une importance particulière est portée sur l'assurance qualité des données produites, en particulier au moment de leur création dans JOSM. Des points de contrôles sont en effet effectués par l'outil avant l'envoi des données sur la plateforme OpenStreetMap.org En complément de ces contrôles génériques, des contrôles complémentaires sont configurés pour le projet :
* les [validateurs Jungle Bus](https://github.com/Jungle-Bus/transport_mapcss) ajoutent de nombreux tests sur la bonne modélisation des données et sur l'exhaustivité des métadonnées
* le greffon PT Assistant ajoute également des [tests](https://wiki.openstreetmap.org/wiki/FR:JOSM/Greffons/PT_Assistant#Tests_disponibles), notamment sur la continuité des tracés
* enfin, des points de contrôle dédiés au projet ont été créés afin de tenir compte des particularités locales du projet

Le détail de la configuration pour activer ces points de contrôle est détaillé sur la [page de wiki du projet](https://wiki.openstreetmap.org/wiki/FR:WikiProject_C%C3%B4te_d%27Ivoire/Transport_Abidjan#Validateurs).

L'essentiel de ces points de contrôles sont également accessibles sur l'outil [Osmose](http://osmose.openstreetmap.fr/fr/errors/?country=ivory_coast&item=9014,1260,2140), qui est un outil communautaire d'assurance qualité largement utilisé par la communauté OpenStreetMap.


## Livrables

À l'issue de cette étape, les données de transport sont disponibles sur OpenStreetMap.org et librement réutilisables par tous.

![701](../images/701.png)

*Un trajet de la ligne 701 opérée par la SOTRA, dans OpenStreetMap*

## Ressources

* [support de la présentation](http://junglebus.io/presentations/Jungle%20Bus%20-%20Sotm-africa%202019%20-%20outils.pdf) réalisée par Jungle Bus au State of the Map Africa sur les outils utilisés pour contribuer des données de transport dans OpenStreetMap
* la [page de wiki du projet](https://wiki.openstreetmap.org/wiki/FR:WikiProject_C%C3%B4te_d%27Ivoire/Transport_Abidjan), qui détaille les tags utilisés ainsi que les éléments de configuration des outils
* [code source de JOSM](https://josm.openstreetmap.de/browser/trunk) sous licence libre GPL v2
* [code source du greffon PT Assistant](https://github.com/JOSM/pt_assistant/) sous licence libre GPL v3
* [code source du pré-réglage Jungle Stops](https://github.com/Jungle-Bus/josm-presets) sous licence libre GPL v3
* [code source de l'outil Voodoo](https://github.com/Jungle-Bus/voodoo) sous licence libre GPL v3 et développé par Jungle Bus
* [code source de l'outil Busy Hours](https://github.com/Jungle-Bus/Busy-Hours) sous licence libre et développé par Jungle Bus
* [code source des validateurs Jungle Bus](https://github.com/Jungle-Bus/transport_mapcss) sous licence libre GPL v3
* [code source des analyses de l'outil Osmose](https://github.com/osm-fr/osmose-backend/) sous licence libre GPL v3 et dont l'essentiel des tests sur les transports ont été développés par Jungle Bus
