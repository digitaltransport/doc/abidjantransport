---
---
# Approche générale

[OpenStreetMap](https://openstreetmap.org/) (OSM) est le socle du travail de production de données réalisé.

OpenStreetMap est « le Wikipédia de la cartographie », une communauté mondiale coordonnée créant des données librement exploitables. Aujourd’hui considérée comme la base de données cartographique ouverte la plus exhaustive au Monde, OpenStreetMap permet notamment la description détaillée des réseaux de transport.

Avec le concours d'[OSM CI](http://openstreetmap.ci/), l'association locale des contributeurs OpenStreetMap du pays, nous avons collecté les informations et avons créé les données de transport dans la base OSM.

Afin d'en faciliter la réutilisation, nous avons également réalisé des exports de ces données depuis la base OSM vers des formats standard.

![approche methodologique](../images/approche-methodologiqe.png)

*Approche méthodologique*

Les pages suivantes détaillent chacune des étapes ainsi que les outils et méthodologies utilisés.

# Ressources

* [une infographie](https://junglebus.io/wp-content/uploads/2019/10/OSM_VS_GTFS_FR.pdf) sur l'utilisation d'OSM et du GTFS pour la cartographie des réseaux de transport

![infographie](../images/infographie.png)