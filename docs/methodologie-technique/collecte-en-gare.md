# Collecte en gare

Les collectes à bord ont été complétés par des relevés statiques effectués dans les gares routières formelles et les gares tête-de-ligne utilisées par les véhicules de transport informel. L'objectif est ici de collecter les horaires précis des départs afin de mesurer le niveau de service de chaque ligne.

![sotra](../images/gare-sotra.png)

*une gare routière de transport formel*

En complément des relevés des horaires, une cartographie détaillée des équipements et des dispositions des différents points de montée et descentes de la gare routière peut également a été réalisée pour les gares principales.


## Outillage

Les collectes horaires ont été réalisées sur papier, à l'aide d'un tableau de la forme suivante :

![tableau](../images/tableau.png)

*tableau pour les relevés des départs*

Dans certaines gares routières, nous avons pu utiliser les relevés des départs et arrivés réalisés par le personnel de la SOTRA.

![releve](../images/releve-gare.png)

*relevé horaire de la ligne 37, fourni par son opérateur*

L'application mobile [Maps.me](https://maps.me/) a été utilisée pour renseigner directement dans OpenStreetMap les équipements (toilettes, commerces, points d'eau) dans les gares routières.


## Livrables

À partir des enquêtes ainsi réalisées, les informations suivantes sur les lignes ont pu être extraites :
* nombre de départ par heure sur une journée type
* détail des équipements dans la gare

![360](../images/360-gare-sud.jpg)
*image 360° capturée lors d'un relevé à la Gare Sud*

## Ressources

* [support de formation aux collecteurs](https://git.digitaltransport4africa.org/data/africa/abidjan/blob/master/Formations/2019-10-10_AbidjanTransport_formation_collecteurs_terrain.pdf)