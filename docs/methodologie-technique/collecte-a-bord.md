# Collecte à bord des véhicules

Afin de disposer des informations nécessaires sur les trajets des transports, nous avons effectué des relevés GPS à bord des véhicules. Une équipe de 7 collecteurs a arpenté le réseau afin de collecter des traces GPS des différents trajets.

<img src="../images/collectrice.jpg" width="300">

*Une collectrice relevant une trace GPS à bord d'un véhicule*


Ces traces correspondent aux différents points de cheminement des véhicules, et contiennent des informations temporelles (heure de passage) et géographiques (coordonnées). Elles peuvent également être annotées pour indiquer des informations utiles ou signaler des points de passages importants (comme les arrêts). Elles sont stockées dans le format standard [GPX](https://fr.wikipedia.org/wiki/GPX_(format_de_fichier)) sur la plateforme [OpenStreetMap.org](https://www.openstreetmap.org/traces/tag/abidjantransport).

Pour chaque ligne de transport identifiée, un relevé de chaque direction a été effectué a minima. Afin de disposer d'informations plus fiables, les lignes du réseau formel ont été collectées une fois en heure de pointe et une fois en heure creuse. Par ailleurs, certaines variantes de lignes (trajet spécial du matin, trajet express) ont également fait l'objet de relevés supplémentaires. Au total, TODO traces GPS ont ainsi été collectées.

## Outillage

Les relevés ont été réalisés avec l'aide de l'application OSM Tracker munis d'un thème dédié.

[OSM Tracker](https://f-droid.org/packages/net.osmtracker/) est une application de relevés GPS pour Android largement utilisée par la communauté OpenStreetMap depuis de nombreuses années et déjà connue des contributeurs locaux.

Jungle Bus a réalisé et traduit en français plusieurs thèmes dédiés à la collecte à bord, permettant d'effectuer les relevés des informations des arrêts de manière uniforme. Cela permet de faciliter l'utilisation de l'application pour les collecteurs tout en minimisant les erreurs lors des relevés. Ces thèmes sont à activer dans les paramètres de l'application avant de commencer la collecte et permettent de disposer de boutons thématiques.

![layout](../images/layout.png)

*Le thème utilisé permet de collecter le niveau de remplissage du véhicule*

Le détail des manipulations à effectuer pour configurer l'application est rappelé sur la [page wiki du projet](https://wiki.openstreetmap.org/wiki/FR:WikiProject_C%C3%B4te_d%27Ivoire/Transport_Abidjan#OSM_Tracker) et les conventions utilisées par les collecteurs dans le cadre du projet sont détaillées dans le [support de formation](https://git.digitaltransport4africa.org/data/africa/abidjan/blob/master/Formations/2019-10-10_AbidjanTransport_formation_collecteurs_terrain.pdf).

## Livrables

À partir des traces GPS ainsi collectées, les informations suivantes sur les lignes ont pu être extraites :
* cheminement des véhicules (trajet emprunté)
* position des arrêts
* nom des arrêts
* niveau de charge des véhicules à l'arrêt
* prix du trajet
* fréquence de passage moyenne estimée du trajet
* durée du trajet
* distance du trajet
* vitesse moyenne de parcours du trajet

![relevé](../images/releve-vitesses.png)

*Relevé des vitesses des véhicules*

## Ressources
* [données collectées](https://www.openstreetmap.org/traces/tag/abidjantransport)
* [récapitulatif des données collectées](https://git.digitaltransport4africa.org/data/africa/abidjan/tree/master/Donn%C3%A9es#traces-gps)
* [support de la formation aux collecteurs](https://git.digitaltransport4africa.org/data/africa/abidjan/blob/master/Formations/2019-10-10_AbidjanTransport_formation_collecteurs_terrain.pdf)
* [code source des thèmes développés pour la collecte des données de transport](https://github.com/Jungle-Bus/osmtracker-layouts) sous licence libre GPL-3.0
* [code source de l'application OSM Tracker](https://github.com/labexp/osmtracker-android) sous licence libre GPL-3.0
