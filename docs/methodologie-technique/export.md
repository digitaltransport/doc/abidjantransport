# Export et réutilisation des données

Une fois les données créées dans OpenStreetMap, elles sont librement réutilisables par tous (sous les conditions de la [licence ODbL](https://www.openstreetmap.org/copyright/fr)).

Cependant, afin de faciliter leur prise en main par les parties prenantes ou les acteurs tiers, nous avons également mis à disposition dans le [centre de ressources DigitalTransport4Africa](https://git.digitaltransport4africa.org/data/africa/abidjan) des exports ou des représentation des données directement réutilisables. 

Un [site vitrine](http://sites.digitaltransport.io/abidjantransport/) rassemblant ces livrables est également disponible pour en faciliter l'accès.

## Exports SIG
Des exports bruts tabulaires (format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values)) et géographiques (format [geojson](https://fr.wikipedia.org/wiki/GeoJSON)) des lignes ont été mis à disposition sur le [centre de ressources DigitalTransport4Africa](https://git.digitaltransport4africa.org/data/africa/abidjan/tree/master/Donn%C3%A9es#exports).
Ces donnés peuvent être manipulées dans des logiciels de tableur (LibreOffice, Excel, etc) ou dans des logiciels de SIG (QGis, ArcGis, etc) afin d'analyser les données ou d'en réaliser des représentations spatiales.
Ces exports ont été réalisés à partir de la base de OpenStreetMap à l'aide du logiciel libre [osm-transit-extractor](https://nlehuby.5apps.com/osm-transit-extractor-couteau-suisse.html).

![extractor](../images/extractor.png)
*Des exports de données prêts à l'emploi*


## Requêtes Overpass

L'API [Overpass](https://wiki.openstreetmap.org/wiki/Overpass_API) est un outil qui permet d'effectuer des requêtes d'extractions personnalisées de données OpenStreetMap.

Nous avons rédigé et mis à disposition sur le centre de ressources DigitalTransport4Africa  des [requêtes Overpass](https://git.digitaltransport4africa.org/data/africa/abidjan/blob/master/Donn%C3%A9es/overpass.md) permettant d'exporter les différents objets de transports (arrêts, lignes, gares routières, etc).

Ces requêtes peuvent également être utilisées dans des outils de visualisation ou de création de carte interactive, comme nous l'avons par exemple illustré lors de la formation aux parties prenantes ; le [support de cette formation](https://git.digitaltransport4africa.org/data/africa/abidjan/blob/master/Formations/2019-11-27_Formation_umap_overpass.pdf) est disponible sur le centre de ressources.

![overpass](../images/overpass.png)

*Carte interactive des arrêts réalisées à partir des requêtes Overpass*

## Exports GTFS

Le [GTFS](https://fr.wikipedia.org/wiki/General_Transit_Feed_Specification) est un format standard pour représenter les données de transport, que ce soit pour des applications d'information voyageur ou des analyses de réseau.

Un GTFS décrivant l'offre de transport formelle et informelle est mis à disposition sur le [centre de ressources DigitalTransport4Africa](https://git.digitaltransport4africa.org/data/africa/abidjan/tree/master/Donn%C3%A9es#donn%C3%A9es-gtfs)

Il a été construit à partir des données OpenStreetMap à partir du logiciel libre osm2gtfs que nous avons fait évolué pour gérer les spécificités du projet.

Le détail des correspondances attributaires entre les informations du GTFS et les informations dans OpenStreetMap sont détaillées dans le support de la formation technique (TODO) sur la production du GTFS.

Afin de s'assurer qu'il est utilisable, ce GTFS est testé à l'aide d'outils libres tels que [FeedValidator](https://github.com/google/transitfeed/wiki/FeedValidator) ou [GTFS-Validator](https://github.com/conveyal/gtfs-validator).

![visualisation](../images/visualisation.png)

*Détail de la ligne 37 dans un outil de visualisation de GTFS*

## Plan schématique

Notre partenaire [Latitude Cartagène](https://latitude-cartagene.com/) a réalisé un [plan schématique du réseau](https://git.digitaltransport4africa.org/data/africa/abidjan/-/raw/master/Plan/plan_schematique.pdf?inline=false) en se basant sur les données produites. Cette entreprise française est spécialisée dans la production de cartes et autres supports d'information voyageurs.

L'objectif de ce plan, qui sera imprimé au format poster, est d'avoir un aperçu de la structuration du réseau en un clin d'œil. Il permet de comprendre l'organisation et la structure du réseau à l'échelle du district, sans pour autant afficher l'intégralité du réseau.

![aperçu du plan](../images/plan_schematique.png)

*Aperçu du plan schématique*

La spécificité d'un plan schématique est de ne pas respecter les dimensions et autres éléments géographiques. La visualisation est centrée sur le réseau de transport en lui-même avant tout.
Une catégorisation des lignes permet d'identifier et de faire ressortir les lignes structurantes à forte fréquence. Une sélection des points d'intérêts les plus importants de chaque commune a également été effectuée afin de pouvoir les afficher sur le plan et en faciliter la lecture. 

De nombreux métiers sont intervenus pour produire ce plan : analyste de données, géographe, cartographe, géomaticien, graphiste ont tous collaborés ensemble pour parvenir à ce résultat à hauteur d'une si grande métropole. Ce plan a été affiné après plusieurs tests auprès d'habitants locaux, fins connaisseurs de leur propre territoire.

## Autres réalisations

Quelques partenaires se sont saisis des données produites pour créer des services autour des transport à Abidjan : carte interactive d'exploration du réseau, application mobile de calcul d'itinéraire, etc

![réalisations partenaire](../images/realisation_partenaires.png)
*Quelques services réalisés par des partenaires en utilisant les données du projet*

Plusieurs démonstrations de ces services ont été réalisées lors de la session de formation de fin de projet en décembre 2020. [Des vidéos ou des captures d'écran](https://git.digitaltransport4africa.org/data/africa/abidjan/-/blob/master/Formations/2020-12-11_d%C3%A9mo/readme.md) sont disponibles sur le centre de ressources.

## Ressources

* [site vitrine présentant tous les livrables](http://sites.digitaltransport.io/abidjantransport/)
* [exports](https://git.digitaltransport4africa.org/data/africa/abidjan/tree/master/Donn%C3%A9es) sur le centre de ressources DigitalTransport4Africa
* [code source d'osm-transit-extractor](https://github.com/CanalTP/osm-transit-extractor) sous licence libre GPL v3
* [code source d'osm2gtfs](https://github.com/grote/osm2gtfs/) sous licence libre GPL v3
