# Implication des parties prenantes

La réussite du projet repose sur l'appropriation locale des méthodes et de la donnée.

Le projet a donc largment reposé sur l'implication des parties prenantes locales et sur leur autonomisation. Ceci est réalisé à travers des formations et par l'hébergement d'OSM-Ci au sein du Ministère des Transports le temps du projet, qui favorise les contacts.

## Formations aux parties prenantes

Afin de faciliter le partage de connaissances, les contributeurs OpenStreetMap réalisant la retranscription des données ont été hébergés dans les locaux du Ministère des Transports. Plusieurs courtes sessions formations à la contribution de données de transport ont ainsi pu être organisées au cours du projet, et le personnel technique du Ministère a ainsi pu leur poser au quotidien toutes les questions utiles et bénéficier de leurs compétences.

En complément, plusieurs sessions de formation formelle ont également organisées avec les différentes parties prenantes durant le projet.

## Initiation à OpenStreetMap

Une demi-journée de formation d'introduction à OpenStreetMap a eu lieu en novembre 2019. Elle était pour tous publics : le personnel du ministère et des différentes communes d'Abidjan ainsi que quelques opérateurs y ont assistées. Cette session était co-animée par Jungle Bus et l'association OpenStreetMap locale et a eu lieu dans les locaux du Ministère.

Après une brève présentation du projet, OpenStreetMap a été présenté. Puis les participants ont appris à rechercher des informations simples dans OpenStreetMap et ont pu réalisé une carte web numérique interactive.

![formation](../images/formation.png)

*Formation de découverte d'OpenStreetMap avec les parties prenantes*

## Formations finales

Pour cloturer le projet, deux jours de formation ont été organisés dans les locaux de l'AMUGA, l'autorité organisatrice des transports d'Abidjan, en décembre 2020. Cette session était co-animée par Jungle Bus, SYSTRA et OpenStreetMap Côte d'Ivoire et toutes les parties prenantes y étaient conviées.

![formation à l'AMUGA](../images/formation_amuga.png)
*Les participants à la session de formation finale du projet*

La première journée était dédiée à un public technique, et avait pour objectif de prendre en main les livrables mais aussi la méthodologie afin de pouvoir reproduire le projet et assurer la maintenance des données.

![plan de la première journée de formation](../images/formation_finale_jour_1.png)
*Programme de la première journée de formation*

La seconde journée était plus ouverte et portait sur les utilisations possibles de ces données dans des projets concrets.

![plan de la deuxième journée de formation](../images/formation_finale_jour_2.png)
*Programme de la deuxième journée de formation*

## Ressources

* [support de toutes les formations réalisées](https://git.digitaltransport4africa.org/data/africa/abidjan/blob/master/Formations/)
