 # Formation des collecteurs

Chaque collecteur a reçu une formation théorique ainsi qu'une formation pratique.

## Formation théorique

La formation d'une demi-journée couvrait les points suivants :
* objectifs du projet
* périmètre de collecte (mode et emprise géographique)
* méthodologie détaillée de la collecte à bord des véhicules, avec les différentes actions à réaliser avant, pendant et après être monté à bord
* méthodologie détaillée de la collecte en gare routière
* mesures de sécurité pendant les opérations de terrain et personnes à prévenir en cas de problème

## Formation pratique

La formation d'une demi-journée se déroulait sur le terrain.

Une visite en gare routière a été réalisée afin de faire manipuler aux collecteurs les outils de collecte statique et d'ajuster la méthodologie.

![gare](../images/terrain-gare.png)

*relevé des horaires de départ des bus à la sortie de la gare Sud, dans la bonne humeur !*

Puis une session de collecte à bord a été réalisée avec tous les collecteurs : une ligne a ainsi été partiellement collectée en même temps par chacun afin de répéter les différentes étapes de la collecte en situation et de répondre à toutes les questions.

![bus](../images/terrain-bus.png)

*Collecte à bord d'un bus de la SOTRA*

## Ressources
* [support de la formation théorique](https://git.digitaltransport4africa.org/data/africa/abidjan/blob/master/Formations/2019-10-10_AbidjanTransport_formation_collecteurs_terrain.pdf)