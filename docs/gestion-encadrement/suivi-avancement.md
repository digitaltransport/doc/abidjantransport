# Suivi d'avancement

Un suivi en continu de l'avancement des relevés a été mis en place.
Un tableur de suivi permet de voir en continu les lignes qui ont été relevées et mappées. Un point hebdomadaire de coordination permet de rendre compte des avancements et préparer le planning de la semaine suivante.
Un groupe whatsApp a été constitué pour le suivi quotidien et la réaction aux éventuels incidents.

## Organisation du planning de collecte

Les collectes ont été organisées à l'avance afin d'assurer la coopération des acteurs du transport.

Une période de temps et une liste nominative de collecteurs a été défini par exemple pour la collecte sur le réseau SOTRA. Ceci a permis à la SOTRA d'émettre des autorisations pour monter dans les bus, ainsi que d'alerter ses machinistes et responsables de lignes.

Pour les collectes statiques en gare SOTRA, la SOTRA a été avertie spécifiquement afin que le responsable de gare donne l'accès aux relevés de départs de bus.

Le Haut Conseil a été averti des collectes Gbakas et Wôrô-wôrô, pour qu'il transmette l'information aux différents syndicats. Les enquêteurs ont payé leur place à bord des bus, ce qui a simplifié le processus.

## Tableur de suivi

Le suivi de l'avancement du projet est orchestré autour d'un tableur partagé entre tous les intervenants.

![tableur](../images/tableur-de-suivi.png)

*tableur de suivi*

Il comprend notamment la liste des listes avec leurs caractéristiques (désignation, mode, origine, destination, etc). Les collecteurs viennent y renseigner l'url des traces GPS qu'ils ont pu collecter et y indiquer des éventuelles notes. Les contributeurs OpenStreetMap en charge de la retranscription viennent à leur tour récupérer les informations des collecteurs et indiquer dans le tableur les lignes retranscrites. Les nouvelles lignes découvertes en cours de projet viennent grossir cette liste de ligne.

## Réunions d'avancement

En phase de collecte active, une réunion de suivi d'avancement hebdomadaire a été mise en place. Elle rassemblait à minima un représentant du groupement SYSTRA - Jungle Bus et un représentant d'OSM-Ci.

Elle aborde à minima les points suivants :
- avancement de la collecte
- avancement du mapping
- revue des incidents
- programmation de la semaine suivante
- tour de table


Des graphiques d'avancement sont réalisés chaque semaine à partir du tableur d'avancement pour mesurer le progrès de la collecte et de la retranscription.

![suivi hebdomadaire](../images/suivi-hebdomadaire.png)
*suivi hebdo de l'avancement de la collecte et de la retranscription*

Un compte-rendu des réunions est transmis au Ministère et accompagné d'un résumé. Le Compte-rendu est également ajouté au [dossier git du projet](https://git.digitaltransport4africa.org/data/africa/abidjan).


## Gestion des incidents

Les collecteurs sont équipés d'une liste de personnes à prévenir en cas d'incident.

Un canal whatsApp est mis en place pour assurer un suivi en temps réel des incidents entre le Groupement SYSTRA - Jungle Bus et OSM-Ci.

Un correspondant est identifié au sein de chaque acteur du transport pour assurer la coordination.

Selon leur gravité, les incidents peuvent être traités à différents niveaux:
- directement entre OSM-Ci et les collecteurs, pour les incidents sans gravité et ayant peu d'impact sur la collecte
- entre OSM-Ci et le groupement SYSTRA - Jungle Bus pour les incidents graves et ceux ayant un impact sur la collecte
- entre le groupement et le représentant de l'acteur transport concerné, pour les incidents nécessitant une action de sa part
- entre le groupement, le Ministère et le responsable de l'acteur de transport concerné pour ceux qui n'ont pas pu être résolus par les moyens ci-dessus

Dans tous les cas, les incidents sont tracés et reportés au Ministère dans le point hebdomadaire pour information.


## Ressources

* [support de formation aux parties prenantes](https://git.digitaltransport4africa.org/data/africa/abidjan/-/blob/master/Formations/2020-12-10_14h45_maintenir_collecte_organisation.pdf) détaillant ces éléments
