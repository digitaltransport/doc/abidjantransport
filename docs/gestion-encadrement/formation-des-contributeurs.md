# Formation des contributeurs OpenStreetMap

Une formation a été réalisée pour trois contributeurs OpenStreetMap par Jungle Bus. Elle se présentait sous la forme d'un atelier de contribution sur la thématique des transports.

![josm arrêt](../images/josm-arret.jpg)

*un arrêt de bus dans JOSM*

Les contributeurs maîtrisaient déjà bien OpenStreetMap, l'outil d'édition [JOSM](https://josm.openstreetmap.de/wiki/Fr%3AWikiStart) et de manière générale l'écosystème OpenStreetMap. Les objectifs de cette formation étaient
* de s'assurer de la bonne compréhension du modèle de données du transport dans OpenStreetMap
* de présenter la différente palette d'outils pour contribuer sur les transports et assurer la qualité des données produites

Le point d'entrée de l'atelier était la [page de wiki du projet](https://wiki.openstreetmap.org/wiki/FR:WikiProject_C%C3%B4te_d%27Ivoire/Transport_Abidjan#Comment_cartographier_.3F) qui détaille notamment les attributs à utiliser dans OpenStreetMap.
Puis chacun a passé un peu de temps dans JOSM pour activer et configurer les différents greffons. Enfin, une ligne de transport qui avait été collectée par les collecteurs durant leur session de formation a été cartographiée.
Enfin, une démonstration des autres outils ([Busy Hours](https://jungle-bus.github.io/Busy-Hours/), [Osmose](http://osmose.openstreetmap.fr/fr/errors/?country=ivory_coast&item=9014,1260,2140), etc) a eu lieu, ainsi qu'un échange de bonnes pratiques sur la cartographie dans JOSM.

## Ressources

* la [page de wiki du projet](https://wiki.openstreetmap.org/wiki/FR:WikiProject_C%C3%B4te_d%27Ivoire/Transport_Abidjan)